package com.example.byronmendoza.practicaswmaterias.rest.Service;

import com.example.byronmendoza.practicaswmaterias.rest.Constanst.ApiConstanst;
import com.example.byronmendoza.practicaswmaterias.rest.Modelo.Materias;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MateriesServices {

    @GET(ApiConstanst.MATERIAS)
    Call<List<Materias>> getMaterias();

    @GET(ApiConstanst.MATERIA)
    Call<Materias> getMateria(@Path("id")String id);
}
