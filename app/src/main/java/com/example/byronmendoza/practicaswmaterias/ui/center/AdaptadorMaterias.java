package com.example.byronmendoza.practicaswmaterias.ui.center;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.byronmendoza.practicaswmaterias.R;
import com.example.byronmendoza.practicaswmaterias.rest.Modelo.Materias;
import com.example.byronmendoza.practicaswmaterias.rest.Service.MateriesServices;

import java.util.ArrayList;

public class AdaptadorMaterias extends RecyclerView.Adapter<AdaptadorMaterias.ViewMaterias>{


    ArrayList<Materias>Mat;

    public AdaptadorMaterias(ArrayList<Materias> Mat){

        this.Mat=Mat;
    }

    @NonNull
    @Override
    public ViewMaterias onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new ViewMaterias(view);


    }

    @Override
    public void onBindViewHolder(@NonNull ViewMaterias holder, final int position) {

        holder.id.setText("ID: "+ Mat.get(position).getId());
        holder.Descripcion.setText("Descripcion: "+ Mat.get(position).getDescripcion());
        holder.Parcial1.setText("Parcial: " + Mat.get(position).getParcial_dos());
        holder.Parcial2.setText("Parcial: " + Mat.get(position).getParcial_dos());
        holder.Aprueba.setText("Aprueba: " +Mat.get(position).getAprueba());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Context context = view.getContext();
                Intent intent = new Intent(context, Main2Activity.class);
                intent.putExtra("id", Mat.get(position).getId());
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return Mat.size();
    }

    public class ViewMaterias extends RecyclerView.ViewHolder{

        TextView id, Descripcion, Parcial1, Parcial2, Aprueba;

        LinearLayout linearLayout;

        public ViewMaterias(View itemView){

            super(itemView);

            id = (TextView)itemView.findViewById(R.id.IdMaterias);
            Descripcion = (TextView)itemView.findViewById(R.id.TxtDescripcion);
            Parcial1 = (TextView)itemView.findViewById(R.id.TxtParcialuno);
            Parcial2 = (TextView)itemView.findViewById(R.id.TxtParcialdos);
            Aprueba = (TextView)itemView.findViewById(R.id.TxtAprueba);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.RlinearMat);
        }
    }
}
