package com.example.byronmendoza.practicaswmaterias.ui.center;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.byronmendoza.practicaswmaterias.R;
import com.example.byronmendoza.practicaswmaterias.rest.Adapter.MateriasAdapter;
import com.example.byronmendoza.practicaswmaterias.rest.Modelo.Materias;

public class Main2Activity extends AppCompatActivity {

    TextView ide2, Descripcion2, parcial12, parcial22, aprueba2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String id = getIntent().getStringExtra("id");

        ide2 = (TextView)findViewById(R.id.IdMaterias2);
        Descripcion2 = (TextView)findViewById(R.id.TxtDescripcion2);
        parcial12 = (TextView)findViewById(R.id.TxtParcial2);
        parcial22 = (TextView)findViewById(R.id.TxtParcial22);
        aprueba2 = (TextView)findViewById(R.id.TxtAprueba2);


        MatMostrar(id);

    }

    private void MatMostrar(String id){
        MateriasAdapter materiasAdapter = new MateriasAdapter();

        Call<Materias> call = materiasAdapter.getMateria(id);
        call.enqueue(new Callback<Materias>() {
            @Override
            public void onResponse(Call<Materias> call, Response<Materias> response) {
                Materias materias = response.body();

                ide2.setText("ID: " + materias.getId().toString());
               Descripcion2.setText("NOMBRES: " + materias.getDescripcion().toString());
                parcial12.setText("PARCIAL 1: " + materias.getParcial_uno().toString());
                parcial22.setText("PARCIAL 2: " + materias.getParcial_dos().toString());
                aprueba2.setText("APRUEBA: " + materias.getAprueba().toString());
            }

            @Override
            public void onFailure(Call<Materias> call, Throwable t) {

            }
        });

    }
}
