package com.example.byronmendoza.practicaswmaterias.rest.Adapter;

import com.example.byronmendoza.practicaswmaterias.rest.Constanst.ApiConstanst;
import com.example.byronmendoza.practicaswmaterias.rest.Modelo.Materias;
import com.example.byronmendoza.practicaswmaterias.rest.Service.MateriesServices;

import java.util.List;

import retrofit2.Call;

public class MateriasAdapter extends BaseAdapter implements MateriesServices{

    public MateriesServices materiesServices;

    public MateriasAdapter() {
        super(ApiConstanst.BASE_MATERIAS_URL);
        materiesServices = createService(MateriesServices.class);
    }

    @Override
    public Call<List<Materias>> getMaterias() {
        return materiesServices.getMaterias();
    }

    @Override
    public Call<Materias> getMateria(String id) {
        return materiesServices.getMateria(id);
    }
}
