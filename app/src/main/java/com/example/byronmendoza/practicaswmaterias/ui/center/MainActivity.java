package com.example.byronmendoza.practicaswmaterias.ui.center;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;

import com.example.byronmendoza.practicaswmaterias.R;
import com.example.byronmendoza.practicaswmaterias.rest.Adapter.MateriasAdapter;
import com.example.byronmendoza.practicaswmaterias.rest.Modelo.Materias;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ArrayList<Materias>ListaMaterias;
    RecyclerView recyclerView;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListaMaterias = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.Rmaterias);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MatMostrar();


    }

    private void MatMostrar(){

        MateriasAdapter materiasAdapter = new MateriasAdapter();
        Call<List<Materias>> call = materiasAdapter.getMaterias();
        call.enqueue(new Callback<List<Materias>>() {
            @Override
            public void onResponse(Call<List<Materias>> call, Response<List<Materias>> response) {
                List<Materias>List = response.body();

                for (Materias materiass: List){

                    Log.e("Materias", materiass.getDescripcion().toString());

                    ListaMaterias.add(materiass);
                }
                AdaptadorMaterias adaptadorEstudiantess = new AdaptadorMaterias(ListaMaterias);
                recyclerView.setAdapter(adaptadorEstudiantess);
            }

            @Override
            public void onFailure(Call<List<Materias>> call, Throwable t) {

            }
        });

    }
}
